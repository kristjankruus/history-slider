﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Windows;
using System.Windows.Resources;
using System.Xml.Linq;
using WorldHistorySlider.Models;
using WorldHistorySlider.Resources;

namespace WorldHistorySlider.ViewModels
{
    public class MainViewModel : INotifyPropertyChanged
    {
        private readonly ObservableCollection<QuizGroup> _quizGroup;

        public ObservableCollection<QuizGroup> QuizGroup
        {
            get { return _quizGroup; }
        }

        public MainViewModel()
        {
            _quizGroup = new ObservableCollection<QuizGroup>();
        }

        public void LoadData()
        {
            XDocument doc = XDocument.Load("Resources\\QuizData.xml");

            using (IsolatedStorageFile isoStore = IsolatedStorageFile.GetUserStoreForApplication())
            {
                if (!isoStore.FileExists("QuizData.xml"))
                {
                    using (IsolatedStorageFileStream isoStream =
                        new IsolatedStorageFileStream("QuizData.xml", FileMode.Create, isoStore))
                    {
                        doc.Save(isoStream);
                    }
                }
            }

            using (IsolatedStorageFile isoStore = IsolatedStorageFile.GetUserStoreForApplication())
            {
                using (IsolatedStorageFileStream isoStream = new IsolatedStorageFileStream("QuizData.xml", FileMode.Open, isoStore))
                {
                    XDocument doc1 = XDocument.Load(isoStream);
                    int id = 0;
                    foreach (var group in doc1.Descendants("QuizGroup").ToList())
                    {
                        if (CheckGroupAnswer(group))
                        {
                            id = Convert.ToInt32(group.Element("id").Value) + 1;
                            group.SetElementValue("answer", true);
                            isoStream.SetLength(isoStream.Length - 1);
                        }
                        if (id == Convert.ToInt32(group.Element("id").Value))
                        {
                            group.SetElementValue("open", true);
                        }
                    }
                    isoStream.Position = 0;
                    isoStream.SetLength(isoStream.Length - 1);
                    doc1.Save(isoStream);
                    var query = from x in doc1.Descendants("QuizGroup") select new QuizGroup(x);
                    foreach (var item in query)
                    {
                        _quizGroup.Add(item);
                    }
                }
            }
        }

        private bool CheckGroupAnswer(XElement group)
        {
            foreach (var quiz in group.Descendants("Quiz"))
            {
                if (!Convert.ToBoolean(quiz.Element("answer").Value))
                {
                    return false;
                }
            }
            return true;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(String propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (null != handler)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}