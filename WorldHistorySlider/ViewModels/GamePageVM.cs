﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Linq;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Resources;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using Microsoft.Devices;
using Microsoft.Phone.Controls.Primitives;
using WorldHistorySlider.Models;

namespace WorldHistorySlider.ViewModels
{
    public class GamePageVM
    {
        private ObservableCollection<QuizModel> _quizModel;
        private enum Items
        {
            January = 1, February = 2, March = 3, April = 4, May = 5, June = 6, July = 7, August = 8, September = 9, October = 10, November = 11, December = 12
        };
        public ObservableCollection<QuizModel> QuizModel
        {
            get { return _quizModel; }
        }

        public GamePageVM()
        {
            _quizModel = new ObservableCollection<QuizModel>();
        }

        public void LoadData()
        {
            foreach (var item in App.Quizgroup.Quizzes)
            {
                _quizModel.Add(item);
            }
        }

        public void CorrectAnswer(UIElementCollection collection, object context)
        {
            foreach (var item in collection)
            {
                if (item.GetType() == typeof(LoopingSelector))
                {
                    var dateValue = (context as QuizModel).Date.Replace(".", "").Substring(collection.IndexOf(item), 1);
                    double test = 0;
                    if (collection.IndexOf(item) == 2 && (context as QuizModel).Date.Contains("."))
                    {
                        Items value;
                        Items.TryParse((context as QuizModel).Date.Replace(".", "")
                            .Substring(collection.IndexOf(item), 2), out value);
                        dateValue = value.ToString();
                    }
                    if (!Double.TryParse(dateValue, out test))
                    {
                        (item as LoopingSelector).DataSource.SelectedItem = dateValue;
                    }
                    else
                    {
                        int number = Convert.ToInt32(dateValue);
                        (item as LoopingSelector).DataSource.SelectedItem = number;
                    }
                    (item as LoopingSelector).IsExpanded = false;
                }
            }
        }

        public bool CheckAnswer(UIElementCollection collection, object context)
        {
            foreach (var item in collection)
            {
                if (item.GetType() == typeof(LoopingSelector))
                {
                    var selValue = (item as LoopingSelector).DataSource.SelectedItem;
                    var dateValue = (context as QuizModel).Date.Replace(".", "").Substring(collection.IndexOf(item), 1);
                    if (selValue.ToString().Length > 1)
                    {
                        dateValue = (context as QuizModel).Date.Replace(".", "").Substring(collection.IndexOf(item), 2);
                        System.Globalization.DateTimeFormatInfo mfi = new System.Globalization.DateTimeFormatInfo();
                        if (!((dateValue.Equals("AD")) || (dateValue.Equals("BC"))))
                        {
                            dateValue = mfi.GetMonthName(Convert.ToInt32(dateValue));
                        }
                    }

                    if (!selValue.ToString().Equals(dateValue))
                    {
                        return false;
                    }
                }
                App.LoopSelector = (item as LoopingSelector);
            }
            return true;
        }

        public void ChangeValue()
        {
            using (IsolatedStorageFile file = IsolatedStorageFile.GetUserStoreForApplication())
            {
                using (IsolatedStorageFileStream stream = new IsolatedStorageFileStream("QuizData.xml", FileMode.Open, file))
                {
                    XDocument doc = XDocument.Load(stream, LoadOptions.None);

                    var test =
                        doc.Descendants("QuizGroup")
                            .Where(x => (int)x.Element("id") == App.Quizgroup.QuizGroupId)
                            .First().Elements("Quiz").Where(x => (int)x.Element("id") == App.Model.QuizId).First();

                    test.SetElementValue("answer", true);

                    stream.Position = 0;
                    stream.SetLength(stream.Length - 1);
                    doc.Save(stream);
                }
            }
        }
    }
}
