﻿using System;
using System.Windows.Controls;
using Microsoft.Phone.Controls.Primitives;

namespace WorldHistorySlider
{
    public class WeekdayDataSource : ILoopingSelectorDataSource
    {
        private int minimum = 1;
        private int maximum = 12;
        private string selectedItem = "January";

        private enum Items
        {
            January = 1, February = 2, March = 3, April = 4, May = 5, June = 6, July = 7, August = 8, September = 9, October = 10, November = 11, December = 12
        };

        public event EventHandler<SelectionChangedEventArgs> SelectionChanged;

        protected virtual void OnSelectedChanged(SelectionChangedEventArgs e)
        {
            var selectionChanged = SelectionChanged;

            if (selectionChanged != null)
                selectionChanged(this, e);

        }

        public object GetNext(object relativeTo)
        {
            var nextValue = Enumvalue(relativeTo.ToString(), 1);
            Object value = (Enumvalue(nextValue <= Maximum ? nextValue : Minimum));
            if (value != null)
            {
                return value.ToString();
            }
            return null;
        }

        public Object Enumvalue(int value)
        {
            foreach (var item in Enum.GetValues(typeof(Items)))
            {
                if (Convert.ToInt32(item) == value)
                {
                    return item;
                }
            }
            return null;
        }
        public int Enumvalue(string value, int number)
        {
            foreach (var item in Enum.GetValues(typeof(Items)))
            {
                if (item.ToString().Equals(value))
                {
                    return (int)item + number;
                }
            }
            return 1;
        }

        public object GetPrevious(object relativeTo)
        {
            var previousValue = Enumvalue(relativeTo.ToString(), -1);

            Object value = (Enumvalue(previousValue >= Minimum ? previousValue : Maximum));
            if (value != null)
            {
                return value.ToString();
            }
            return null;
        }

        public object SelectedItem
        {
            get
            {
                return selectedItem;
            }
            set
            {
                var oldValue = selectedItem;

                var newValue = value;

                if (oldValue == newValue.ToString())
                    return;

                selectedItem = newValue.ToString();

                OnSelectedChanged(new SelectionChangedEventArgs(new[] { oldValue }, new[] { newValue }));
            }
        }

        public int Minimum
        {
            get
            {
                return minimum;
            }
            set
            {
                minimum = value;

                if (Convert.ToInt32(selectedItem) < minimum)
                    SelectedItem = value;
            }
        }


        public int Maximum
        {
            get
            {
                return maximum;
            }
            set
            {
                maximum = value;

                if (Convert.ToInt32(selectedItem.ToString()) > maximum)
                    SelectedItem = value;
            }
        }
    }
}
