﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using Microsoft.Phone.Controls.Primitives;

namespace WorldHistorySlider
{
   public class DateDataSource:ILoopingSelectorDataSource
    {
        private object selectedItem="AD";
        public object GetNext(object relativeTo)
        {
            if (relativeTo.ToString().Equals("AD"))
            {
                return "BC";
            }
            else
            {
                return null;
            }
        }

        protected virtual void OnSelectedChanged(SelectionChangedEventArgs e)
        {
            var selectionChanged = SelectionChanged;

            if (selectionChanged != null)
                selectionChanged(this, e);

        }

        public object GetPrevious(object relativeTo)
        {
            if (relativeTo.ToString().Equals("AD"))
            {
                return null;
            }
            else
            {
                return "AD";
            }
        }

        public object SelectedItem
        {
            get
            {
                return selectedItem;
            }
            set
            {
                var oldValue = selectedItem;
                var newValue=value;
                if (oldValue.ToString().Equals("AD"))
                {
                    newValue = "BC";
                }
                else
                {
                    newValue = "AD";
                }

                if (oldValue.ToString() == newValue.ToString())
                    return;

                selectedItem = newValue;

                OnSelectedChanged(new SelectionChangedEventArgs(new[] { oldValue }, new[] { newValue }));
                
            }
        }

        public event EventHandler<System.Windows.Controls.SelectionChangedEventArgs> SelectionChanged;
    }
}
