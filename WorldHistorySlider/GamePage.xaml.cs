﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Controls.Primitives;
using WorldHistorySlider.Models;
using WorldHistorySlider.ViewModels;

namespace WorldHistorySlider
{
    public partial class GamePage : PhoneApplicationPage
    {
        GamePageVM vm = new GamePageVM();
        private LoopingSelector Looper;
        public GamePage()
        {
            InitializeComponent();
            Loaded += GamePage_Loaded;
        }

        void GamePage_Loaded(object sender, RoutedEventArgs e)
        {
            vm.LoadData();
            DataContext = vm;
            App.Model = (Pivot.Items.ElementAt(0) as QuizModel);
        }

        private void Pivot_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            App.Model = (Pivot.SelectedItem as QuizModel);
        }

        private void UIElement_OnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            UIElementCollection collection = (sender as StackPanel).Children;

            var test = (sender as StackPanel).DataContext;
            if (vm.CheckAnswer(collection, test))
            {
                var stack = (sender as StackPanel);
                var answer = ((sender as StackPanel).Parent as StackPanel).Children.ElementAt(3) as Image;

                vm.ChangeValue();
                stack.IsHitTestVisible = false;
                stack.Margin = new Thickness(0, -40, 0, 0);
                answer.Source = new BitmapImage(new Uri("Assets/save.png", UriKind.Relative));
            }
            var oo = App.LoopSelector;
            if (oo != null)
            {
                oo.IsExpandedChanged += oo_IsExpandedChanged;
            }
        }

        void oo_IsExpandedChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            (sender as LoopingSelector).IsExpanded = false;
        }

        private void FrameworkElement_OnLoaded(object sender, RoutedEventArgs e)
        {
            UIElementCollection collection = (sender as StackPanel).Children;
            if (!(sender as StackPanel).IsHitTestVisible)
            {
                var test = (sender as StackPanel).DataContext;
                vm.CorrectAnswer(collection, test);
            }
        }

        private void UIElementa_OnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            UIElement_OnMouseLeftButtonDown((sender as LoopingSelector).Parent, e);

            if (Looper != null && (sender as LoopingSelector) != Looper)
            {
                Looper.IsExpanded = false;
            }
            Looper = (sender as LoopingSelector);
        }
    }
}