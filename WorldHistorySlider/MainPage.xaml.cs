﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Microsoft.Advertising;
using WorldHistorySlider.Models;
using WorldHistorySlider.ViewModels;

namespace WorldHistorySlider
{
    public partial class MainPage
    {
        // Constructor
        public MainPage()
        {
            InitializeComponent();
            Loaded += MainPage_Loaded;
        }

        void MainPage_Loaded(object sender, RoutedEventArgs e)
        {
            MainViewModel vm = new MainViewModel();
            vm.LoadData();
            DataContext = vm;
        }


        // Handle selection changed on LongListSelector
        private void MainLongListSelector_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            // If selected item is null (no selection) do nothing
            if (MainLongListSelector.SelectedItem == null)
                return;
            if ((MainLongListSelector.SelectedItem as QuizGroup).Open)
            {
                App.Quizgroup = MainLongListSelector.SelectedItem as QuizGroup;
                // Navigate to the new page
                NavigationService.Navigate(new Uri("/GamePage.xaml", UriKind.Relative));
            }
            // Reset selected item to null (no selection)
            MainLongListSelector.SelectedItem = null;
        }
    }
}