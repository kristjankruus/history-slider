﻿using System;
using System.Windows;
using System.Windows.Data;
using WorldHistorySlider.Models;

namespace WorldHistorySlider.Common
{
    public class ListToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            bool group = System.Convert.ToBoolean(value);

            if (group == false)
            {
                return "/Assets/add.png";
                // return inverse ? Visibility.Collapsed : Visibility.Visible;
            }
            return "/Assets/ada.png";
            //   return inverse ? Visibility.Visible : Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return value;
        }
    }
}
