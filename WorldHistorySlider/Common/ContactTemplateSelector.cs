﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using WorldHistorySlider.Models;

namespace WorldHistorySlider.Common
{
    public class ContactTemplateSelector:DataTemplateSelector 
    {
        public DataTemplate Date
        {
            get;
            set;
        }

        public DataTemplate Year
        {
            get;
            set;
        }
        public DataTemplate YearBC
        {
            get;
            set;
        }

        public DataTemplate YearBCM
        {
            get;
            set;
        }
        public DataTemplate YearBCTM
        {
            get;
            set;
        }

        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            var contact = item as QuizModel;
            if (contact != null)
            {
                if (contact.Type.Equals("date"))
                {
                    return Date;
                }
                else if (contact.Type.Equals("yearBC"))
                {
                    return Year;
                }
                else if (contact.Type.Equals("yearBCM"))
                {
                    return YearBCM;
                }
                else if (contact.Type.Equals("yearBCTM"))
                {
                    return YearBCTM;
                }
                else
                {
                    return YearBC;
                }
            }

            return base.SelectTemplate(item, container);
        }
    }
}
