﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Xml.Linq;

namespace WorldHistorySlider.Models
{
    public class QuizModel
    {
        public QuizModel(XElement item)
        {
            QuizId = Convert.ToInt32((item.Element("id").Value));
            Name = item.Element("name").Value;
            Type = item.Element("type").Value;
            QuizImage = item.Element("img").Value;
            Date = item.Element("date").Value;
            Answered = Convert.ToBoolean(item.Element("answer").Value);
        }

        public int QuizId { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string Description { get; set; }
        public string QuizImage { get; set; }
        public string Date { get; set; }
        public bool Answered { get; set; }
    }
}
