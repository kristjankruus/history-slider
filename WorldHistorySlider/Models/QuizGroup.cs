﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Xml.Linq;

namespace WorldHistorySlider.Models
{
    public class QuizGroup
    {
        public int QuizGroupId { get; set; }
        public bool Open { get; set; }
        public bool Answered { get; set; }
        public string Name { get; set; }
        public string Img { get; set; }
        public List<QuizModel> Quizzes { get; set; }

        public QuizGroup(XElement item)
        {
            QuizGroupId = Convert.ToInt32((item.Element("id").Value));
            Name = item.Element("name").Value;
            Open = Convert.ToBoolean(item.Element("open").Value);
            Answered = Convert.ToBoolean(item.Element("answer").Value);
            Img = item.Element("img").Value;
            Quizzes = Models(item.Elements("Quiz").ToList());
        }

        private List<QuizModel> Models(List<XElement> list)
        {
            List<QuizModel> models = new List<QuizModel>();
            foreach (var item in list)
            {
                models.Add(new QuizModel(item));
            }
            return models;
        }
    }
}
